<div class="col-lg-3">
	<aside class="sidebar">
<!-- 		
		<form id="cari" action="<?php echo base_url(); ?>index.php/cari" method="get">
			<div class="input-group input-group-4">
				<input class="form-control" placeholder="Search..." name="keyword" id="keyword" type="text">
				<span class="input-group-btn">
					<button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-search"></i></button>
				</span>
			</div>
        </form> -->

		<hr>
        
        <!-- belum ada isi -->

		<div class="tabs mb-5">
			<ul class="nav nav-tabs">
				<li class="nav-item active"><a class="nav-link" href="#newPosts" data-toggle="tab"><i class="fa fa-star"></i> Terbaru</a></li>
				<!-- <li class="nav-item"><a class="nav-link" href="#recentPosts" data-toggle="tab">Recent</a></li> -->
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="newPosts">
					<ul class="simple-post-list">
					<?php
	                if($all!==false) {
	                	$i = 1;
						foreach($all as $row) { ?>
						<li>
							<div class="post-image">
								<div class="img-thumbnail">
									<a href="<?php echo base_url() ?>berita/post/<?= $row->beritaNama ?>">
										<img style="width:30px;height:30px;" src="<?php echo base_url('berita/loadthumb/') . $row->beritaBanner ?>" alt="">
									</a>
								</div>
							</div>
							<div class="post-info">
								<a href="<?php echo base_url() ?>berita/post/<?= $row->beritaNama ?>"><?=$row->beritaJudul?></a>
								<div class="post-meta">
									<?=$row->beritaDatetime?>
								</div>
							</div>
						</li>
						<?php }
				    }?>
						
					</ul>
				</div>
				
			</div>
		</div>

    </aside>
</div>