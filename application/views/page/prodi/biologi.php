<div role="main" class="main">

	<section class="page-header">
		<div class="container">
			<div class="row">
				<div class="col">
					<ul class="breadcrumb">
						<li><a href="">Jurusan</a></li>
						<li class="active">Program Studi</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<h1>Biologi</h1>
				</div>
			</div>
		</div>
    </section>
    
    <div class="container">

        <div class="row">
            <div class="col-lg-3">
                <aside class="sidebar">
                    <h4 class="heading-primary"><strong>Filter</strong> By</h4>

                    <ul class="nav nav-list flex-column mb-4 sort-source" data-sort-id="portfolio" data-option-key="filter" data-plugin-options="{'layoutMode': 'fitRows', 'filter': '*'}">
                        <li class="nav-item" data-option-value="*"><a class="nav-link active" href="#">Show All</a></li>
                        <li class="nav-item" data-option-value=".deskripsi"><a class="nav-link" href="#">Deskripsi</a></li>
                        <li class="nav-item" data-option-value=".visi"><a class="nav-link" href="#">Visi</a></li>
                        <li class="nav-item" data-option-value=".misi"><a class="nav-link" href="#">Misi</a></li>
                        <li class="nav-item" data-option-value=".makul"><a class="nav-link" href="#">Mata Kuliah</a></li>
                        <li class="nav-item" data-option-value=".dosen"><a class="nav-link" href="#">Dosen</a></li>
                        <li class="nav-item" data-option-value=".jurnal"><a class="nav-link" href="#">Jurnal</a></li>
                        <li class="nav-item" data-option-value=".lab"><a class="nav-link" href="#">Laboratorium</a></li>
                        <li class="nav-item" data-option-value=".alumni"><a class="nav-link" href="#">Alumni</a></li>
                        <li class="nav-item" data-option-value=".web"><a class="nav-link" href="#">Website</a></li>
                    </ul>

                    <hr class="invisible mt-5 mb-2">

                </aside>
            </div>
            <div class="col-lg-9">

                <div class="sort-destination-loader sort-destination-loader-showing">
                    <div class="row portfolio-list sort-destination" data-sort-id="portfolio">
                        <div class="col-lg-12 isotope-item deskripsi">
                            <div class="portfolio-item">
                                <p>Akreditasi : A<br>
    Nomor : 3417/SK/BAN-PT/Akred/S/IX/2019<br>
    Program Studi Biologi mendukung visi universitas dan fakultas menjadi program studi yang unggul di tingkat nasional dalam bidang biologi dan mampu berkompetisi dalam skala internasional serta bermakna bagi masyarakat dengan mengembangkan subjek-subjek perkuliahan ilmu biologi (botani, zoologi, mikrobiologi, bioteknologi, ekologi, dan lingkungan) berkarakter konservasi, membekali mahasiswa agar mampu meneliti khususnya bidang penelitian biologi, dan bioenterpreneurship.<br>
    Program Studi Biologi menyelenggarakan pembelajaran berbasis kompetensi(Kompetensi Utama, Pendukung, dan Kompetensi Lainnya) untuk menghasilkan lulusan dengan kualifikasi sarjana (S1) yang unggul, professional, terampil, dan peka terhadap konservasi lingkungan dan sosial-budaya. Lulusan Program Studi Biologi juga dibekali  bioenterpreneurship. Persyaratan lulus pada program studi bila mahahasiswa telah menyelesaikan mata kuliah minimal 144 SKS dengan IPK minimal 2.00 dengan tidak memiliki nilai D lebih dari 2 mata kuliah. Gelar yang diperoleh untuk para lulusan adalah Sarjana Sarjana Sains (S.Si).</p>
                            </div>
                        </div>
                        <div class="col-lg-12 isotope-item visi">
                            <div class="portfolio-item">
                            <p>Visi<br>
                            Menuju Program Studi Biologi yang menerapkan konsep Biologi modern berbasis biodiversitas hutan tropika basah dataran rendah dan lingkungannya untuk mendukung perkembangan ilmu-ilmu terapan dalam memenuhi kebutuhan dan kesejahteraan manusia.</p>
                            </div>
                        </div>
                        <div class="col-lg-12 isotope-item misi">
                            <div class="portfolio-item">
                            <p>Misi<br>
                            Melayani masyarakat Kalimantan Timur, warga negara Indonesia dan umat manusia melalui program-program terbaik untuk :<br>

1. Mendidik generasi muda menjadi sarjana Biologi untuk menjunjung tinggi etika yang menghargai hak hidup semua bentuk kehidupan dan mampu bekerjasama di lingkungannya.<br>
2. Berperan sebagai institusi pelopor pendidikan dan penelitian biologi hutan tropika basah dataran rendah dan lingkungannya agar mampu menggali dan menemukan pengetahuan baru, serta berpartisipasi aktif dalam mengembangkan ide dan konsep tersebut untuk mendukung perkembangan ilmu-ilmu terapan.<br>
3. Melayani program pendidikan dan keahlian Biologi sepanjang hayat kepada masyarakat dan sebagai pelopor dalam membantu memecahkan masalah Biologi umat manusia.</p>
                            
                            </div>
                        </div>
                        
                    </div>
                </div>

            </div>

            
        </div>

    </div>

	<!-- <div class="container">

		<div class="row">
			<div class="col-lg-6">
				<div class="blog-posts single-post">
					<article class="post post-large blog-single-post">
						<div class="post-content">	
							<p>Akreditasi : A<br>
Nomor : 3417/SK/BAN-PT/Akred/S/IX/2019<br>
Program Studi Biologi mendukung visi universitas dan fakultas menjadi program studi yang unggul di tingkat nasional dalam bidang biologi dan mampu berkompetisi dalam skala internasional serta bermakna bagi masyarakat dengan mengembangkan subjek-subjek perkuliahan ilmu biologi (botani, zoologi, mikrobiologi, bioteknologi, ekologi, dan lingkungan) berkarakter konservasi, membekali mahasiswa agar mampu meneliti khususnya bidang penelitian biologi, dan bioenterpreneurship.<br>
Program Studi Biologi menyelenggarakan pembelajaran berbasis kompetensi(Kompetensi Utama, Pendukung, dan Kompetensi Lainnya) untuk menghasilkan lulusan dengan kualifikasi sarjana (S1) yang unggul, professional, terampil, dan peka terhadap konservasi lingkungan dan sosial-budaya. Lulusan Program Studi Biologi juga dibekali  bioenterpreneurship. Persyaratan lulus pada program studi bila mahahasiswa telah menyelesaikan mata kuliah minimal 144 SKS dengan IPK minimal 2.00 dengan tidak memiliki nilai D lebih dari 2 mata kuliah. Gelar yang diperoleh untuk para lulusan adalah Sarjana Sarjana Sains (S.Si).</p>
							<i class="fa fa-tag">Program Studi Biologi</i> 
						</div>
					</article>

				</div>
			</div>

			<!-- <?php 
			$this->load->view('page/sidebar'); 
			?> -->
		</div>

	</div> -->


</div>