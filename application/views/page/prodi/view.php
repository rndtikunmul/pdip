<div role="main" class="main">

	<section class="page-header">
		<div class="container">
			<div class="row">
				<div class="col">
					<ul class="breadcrumb">
						<li class="active">Jurusan</li>
						<li class="active">Prodi</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<h1><?= $datas->prodiNama?></h1>
				</div>
			</div>
		</div>
	</section>

	<div class="container">

		<div class="row">

			<div class="col-lg-3">
                <aside class="sidebar">

                    <ul class="nav nav-list flex-column mb-4 sort-source" data-sort-id="portfolio" data-option-key="filter" data-plugin-options="{'layoutMode': 'fitRows', 'filter': '*'}">
                        <li class="nav-item" data-option-value="*"><a class="nav-link active" href="#">Semua</a></li>
                        <li class="nav-item" data-option-value=".profil"><a class="nav-link" href="#">Profil</a></li>
                        <ul>
                            <li class="nav-item"><a class="nav-link" href="#">Deskripsi</a></li>
                            <li class="nav-item"><a class="nav-link" href="#">Visi, Misi, dan Tujuan</a></li>
                        </ul>
                        <li class="nav-item" data-option-value=".pendidikan"><a class="nav-link" href="#">Pendidikan</a></li>
                        <ul>
                            <li class="nav-item"><a class="nav-link" href="#">Kurikulum</a></li>
                            <li class="nav-item"><a class="nav-link" href="#">Kegiatan pembelajaran</a></li>
                        </ul>
                        <li class="nav-item" data-option-value=".penelitian"><a class="nav-link" href="#">Penelitian dan Pengabdian kepada masyarakat</a></li>
                        <ul>
                            <li class="nav-item"><a class="nav-link" href="#">Daftar penelitian dan publikasi</a></li>
                            <li class="nav-item"><a class="nav-link" href="#">Daftar pengabdian masyarakat</a></li>
                        </ul>
                        <li class="nav-item" data-option-value=".download"><a class="nav-link" href="#">Download</a></li>
                        <ul>
                            <li class="nav-item"><a class="nav-link" href="#">Panduan</a></li>
                            <li class="nav-item"><a class="nav-link" href="#">Jadwal</a></li>
                            <li class="nav-item"><a class="nav-link" href="#">Buku modul, praktikum, keterampilan medik</a></li>
                            <li class="nav-item"><a class="nav-link" href="#">Formulir</a></li>
                        </ul>
                        <!-- <li class="nav-item" data-option-value=".makul"><a class="nav-link" href="#">Mata Kuliah</a></li>
                        <li class="nav-item" data-option-value=".dosen"><a class="nav-link" href="#">Dosen</a></li>
                        <li class="nav-item" data-option-value=".jurnal"><a class="nav-link" href="#">Jurnal</a></li>
                        <li class="nav-item" data-option-value=".lab"><a class="nav-link" href="#">Laboratorium</a></li>
                        <li class="nav-item" data-option-value=".alumni"><a class="nav-link" href="#">Alumni</a></li>
                        <li class="nav-item" data-option-value=".web"><a class="nav-link" href="#">Website</a></li> -->
                    </ul>

                    <hr class="invisible mt-5 mb-2">

                </aside>
			</div>
			
			<div class="col-lg-9">

                <div class="sort-destination-loader sort-destination-loader-showing">
                    <div class="row portfolio-list sort-destination" data-sort-id="portfolio">
                        <div class="col-lg-12 isotope-item profil">
                            <div class="portfolio-item">
                                <h3 class="heading-primary"><strong>Profil</strong> <?= $datas->prodiNama?></h3>
                                <div class="accordion" id="accordion">
                                    <div class="card card-default">
                                        <div class="card-header">
                                            <h4 class="card-title m-0">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse1One">
                                                    Deskripsi
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse1One" class="collapse show">
                                            <div class="card-body">
                                                <?= $datas->prodiDesk?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card card-default">
                                        <div class="card-header">
                                            <h4 class="card-title m-0">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse1Two">
                                                    Visi
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse1Two" class="collapse">
                                            <div class="card-body">
                                                <?= $datas->prodiVisi?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card card-default">
                                        <div class="card-header">
                                            <h4 class="card-title m-0">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse1Three">
                                                    Misi
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse1Three" class="collapse">
                                            <div class="card-body">
                                                <?= $datas->prodiMisi?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card card-default">
                                        <div class="card-header">
                                            <h4 class="card-title m-0">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse1Four">
                                                    Tujuan
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse1Four" class="collapse">
                                            <div class="card-body">
                                                <?= $datas->prodiTujuan?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 isotope-item pendidikan">
                            <div class="portfolio-item">
                                <h3 class="heading-primary"><strong>Pendidikan</strong> <?= $datas->prodiNama?></h3>
                                <div class="accordion" id="accordion2">
                                    <div class="card card-default">
                                        <div class="card-header">
                                            <h4 class="card-title m-0">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse2One">
                                                    Kurikulum
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse2One" class="collapse show">
                                            <div class="card-body">
                                                <?= $datas->prodiKurikulum?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card card-default">
                                        <div class="card-header">
                                            <h4 class="card-title m-0">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse2Two">
                                                    Kegiatan Pembelajaran
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse2Two" class="collapse">
                                            <div class="card-body">
                                                <?= $datas->prodiKegiatan?>
                                            </div>
                                        </div>
                                    </div>                                    
                                </div>                          
                            </div>
                        </div>
                        <div class="col-lg-12 isotope-item penelitian">
                            <div class="portfolio-item">
                                <h3 class="heading-primary"><strong>Penelitian dan Pengabdian kepada masyarakat</strong></h3>
                                <div class="accordion" id="accordion3">
                                    <div class="card card-default">
                                        <div class="card-header">
                                            <h4 class="card-title m-0">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse3One">
                                                    Daftar Penelitian dan Publikasi
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse1One" class="collapse show">
                                            <div class="card-body">
                                                <?= $datas->prodiPenelitian?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card card-default">
                                        <div class="card-header">
                                            <h4 class="card-title m-0">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse3Two">
                                                    Daftar Pengabdian Masyarakat
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse3Two" class="collapse">
                                            <div class="card-body">
                                                <?= $datas->prodiPengabdian?>
                                            </div>
                                        </div>
                                    </div>                                    
                                </div>                            
                            </div>
                        </div>
                        <div class="col-lg-12 isotope-item download">
                            <div class="portfolio-item">
                                <h3 class="heading-primary"><strong>Download</strong></h3>
                                <div class="accordion" id="accordion4">
                                    <div class="card card-default">
                                        <div class="card-header">
                                            <h4 class="card-title m-0">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion4" href="#collapse4One">
                                                    Panduan
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse1One" class="collapse show">
                                            <div class="card-body">
                                                <?= $datas->prodiPanduan?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card card-default">
                                        <div class="card-header">
                                            <h4 class="card-title m-0">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion4" href="#collapse4Two">
                                                    Jadwal
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse1Two" class="collapse">
                                            <div class="card-body">
                                                <?= $datas->prodiJadwal?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card card-default">
                                        <div class="card-header">
                                            <h4 class="card-title m-0">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion4" href="#collapse4Three">
                                                    Buku Modul, Praktikum, dan Keterampilan medik
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse4Three" class="collapse">
                                            <div class="card-body">
                                                <?= $datas->prodiBuku?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card card-default">
                                        <div class="card-header">
                                            <h4 class="card-title m-0">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion4" href="#collapse4Four">
                                                    Formulir
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse4Four" class="collapse">
                                            <div class="card-body">
                                                <?= $datas->prodiFormulir?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="col-lg-12 isotope-item lab">
                            <div class="portfolio-item">
                                <p><strong>Laboratorium</strong></p>
                                <?php if (!empty($lab)) {
                                    foreach ($lab as $row) : ?>
                                        <a href="<?php echo base_url() ?>lab/post/<?= $row->labId ?>"><?= $row->labNama?></p>
                                        <?php endforeach;
                                    } ?>
                            </div>
                        </div> -->
                        
                    </div>
                </div>

            </div>
		</div>

	</div>


</div>