<div role="main" class="main">

	<section class="page-header">
		<div class="container">
			<div class="row">
				<div class="col">
					<ul class="breadcrumb">
						<li><a href="#">Galeri</a></li>
						<li class="active">Foto</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<h1>Daftar Foto</h1>
				</div>
			</div>
		</div>
	</section>

	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<aside class="sidebar" id="sidebar" data-plugin-sticky data-plugin-options="{'minWidth': 991, 'containerSelector': '.container', 'padding': {'top': 110}}">

					<h4 class="heading-primary"><strong>Filter</strong> By</h4>

					<ul class="nav nav-list flex-column mb-4 sort-source" data-sort-id="portfolio" data-option-key="filter" data-plugin-options="{'layoutMode': 'fitRows', 'filter': '*'}">
						<li class="nav-item" data-option-value="*"><a class="nav-link active" href="#">Show All</a></li>
						<?php
						if($datafolder!==false) {
							foreach($datafolder as $row) { ?>
								<li class="nav-item" data-option-value=".<?=$row->headgalfId?>"><a class="nav-link" href="#"><?=$row->headgalfNama?></a></li>
							<?php }
						}?>
					</ul>

				</aside>
			</div>
			<div class="col">
				<div class="row image-gallery sort-destination lightbox" data-sort-id="portfolio" data-plugin-options="{'delegate': 'a.lightbox-portfolio', 'type': 'image', 'gallery': {'enabled': true}}">
					<?php
					if($data!==false) {
						$i = 1;
						foreach($data as $row) { ?>
							<div class="col-sm-6 col-lg-3 isotope-item websites mb-4 mb-lg-0 <?=$row->headgalfId?>">
								<div class="image-gallery-item">
									<a href="<?php echo base_url('galeri/loadimage/').$row->galfFiles?>" class="lightbox-portfolio">
											<span class="thumb-info">
												<span class="thumb-info-wrapper">
													<img style="height:150px" src="<?php echo base_url('galeri/loadimage/').$row->galfFiles?>" class="img-fluid" alt="">
													<span class="thumb-info-title">
														<span class="thumb-info-type"><?=$row->galfNama?></span> 
													</span>
												</span>
											</span>
										</a>
									</div>
								</div>
							<?php }
						}?>
					</div>
				</div>
			</div>
		</div>

	</div>

	