<div role="main" class="main">

	<section class="page-header">
		<div class="container">
			<div class="row">
				<div class="col">
					<ul class="breadcrumb">
						<li class="active">Laboratorium</li>
						<li class="active"><?= $datas->prodiNama?></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<h1><?= $datas->labNama?></h1>
				</div>
			</div>
		</div>
	</section>

	<div class="container">

		<div class="row">

			<div class="col-lg-3">
                <aside class="sidebar">

                    <ul class="nav nav-list flex-column mb-4 sort-source" data-sort-id="portfolio" data-option-key="filter" data-plugin-options="{'layoutMode': 'fitRows', 'filter': '*'}">
                        <li class="nav-item" data-option-value="*"><a class="nav-link active" href="#">Semua</a></li>
                        <li class="nav-item" data-option-value=".deskripsi"><a class="nav-link" href="#">Deskripsi</a></li>
                        <li class="nav-item" data-option-value=".visi"><a class="nav-link" href="#">Visi</a></li>
                        <li class="nav-item" data-option-value=".misi"><a class="nav-link" href="#">Misi</a></li>                        
                        <li class="nav-item" data-option-value=".lab"><a class="nav-link" href="#">Laboratorium</a></li>
                    </ul>

                    <hr class="invisible mt-5 mb-2">

                </aside>
			</div>
			
			<div class="col-lg-9">

                <div class="sort-destination-loader sort-destination-loader-showing">
                    <div class="row portfolio-list sort-destination" data-sort-id="portfolio">
                        <div class="col-lg-12 isotope-item deskripsi">
                            <div class="portfolio-item">
                                <p><?= $datas->labDesk?></p>
                            </div>
                        </div>
                        <div class="col-lg-12 isotope-item visi">
                            <div class="portfolio-item">
                            <p>Visi<br>
                            <?= $datas->labVisi?></p>
                            </div>
                        </div>
                        <div class="col-lg-12 isotope-item misi">
                            <div class="portfolio-item">
                            <p>Misi<br>
                            <?= $datas->labMisi?></p>
                            
                            </div>
                        </div>
                        
                    </div>
                </div>

            </div>
		</div>

	</div>


</div>