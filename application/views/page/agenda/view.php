<div role="main" class="main">

    <section class="page-header">
        <div class="container">
            <div class="row">
                <div class="col">
                    <ul class="breadcrumb">
                        <li><a href="<?php echo base_url()?>">Home</a></li>
                        <li class="active">Ragam Informasi</li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h1>Berita & Kegiatan</h1>
                </div>
            </div>
        </div>
    </section>

    <div class="container">

        <div class="row">
            <div class="col-lg-9">
                <div class="blog-posts">
                <?php
	                if($agenda!==false) {
	                	$i = 1;
						foreach($agenda as $row) { ?>

                    <article class="post post-medium">
                        <div class="row">

                            <div class="col-lg-5">
                                <div class="post-image">
                                        <div>
                                            <div class="img-thumbnail">
                                                <img class="img-fluid" src="<?php echo base_url('agenda/loadthumb/') . $row->beritaBanner ?>" alt="">
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <div class="col-lg-7">

                                <div class="post-content">

                                    <h2><a href="<?php echo base_url() ?>agenda/post/<?= $row->beritaNama ?>"><?=$row->beritaJudul?></a></h2>
                                    <p><?= substr(strip_tags($row->beritaContent), 0, 200) . '...'; ?></p>

                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="post-meta">
                                    <span><i class="fa fa-calendar"></i> <?=$row->beritaDatetime?> </span>
                                    <span><i class="fa fa-user"></i> By <a ><?=$row->beritaAuthor?></a> </span>
                                    <span><i class="fa fa-tag"></i> <a ><?=$row->beritaTag?></a> </span>
                                    <span class="d-block d-md-inline-block float-md-right mt-3 mt-md-0"><a href="<?php echo base_url() ?>berita/post/<?= $row->beritaNama ?>" class="btn btn-xs btn-primary">Read more...</a></span>
                                </div>
                            </div>
                        </div>

                    </article>
                    <?php }
				    }?>
                    
                </div>
            </div>

            <?php
            $this->load->view('page/sidebar');
            ?>
        </div>

    </div>

</div>