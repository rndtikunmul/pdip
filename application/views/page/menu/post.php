<div role="main" class="main">

	<section class="page-header">
		<div class="container">
			<div class="row">
				<div class="col">
					<ul class="breadcrumb">
						<li><a href=""><?= $datas->pageHead?></a></li>
						<li class="active"><?= $datas->pageJudul?></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<h1><?= $datas->pageJudul?></h1>
				</div>
			</div>
		</div>
	</section>

	<div class="container">

		<div class="row">
			<div class="col-lg-9">
				<div class="blog-posts single-post">
					<article class="post post-large blog-single-post">
						<div class="post-content">	
							<p><?= $datas->pageContent?></p>
							<i class="fa fa-tag"> <?= $datas->pageTag?></i> 
						</div>
					</article>

				</div>
			</div>

			<?php 
			$this->load->view('page/sidebar'); 
			?>
		</div>

	</div>


</div>