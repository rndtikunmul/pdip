<footer id="footer">
    <div class="container">
        <div class="row">
            <!-- <div class="col-lg-3">
                <div class="newsletter">
                    <h4>Newsletter</h4>
                    <p>Keep up on our always evolving product features and technology. Enter your e-mail and subscribe to our newsletter.</p>

                </div>
            </div> -->
            <div class="col-lg-4">
                <h4>Map</h4>
                <div>
                    <iframe src=<?= $footer->footMap ?> width="230" height="120" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="contact-details">
                    <h4>Contact Us</h4>
                    <ul class="contact">
                        <li>
                            <p><i class="fa fa-map-marker"></i> <strong>Address:</strong> <?= $footer->footAlamat ?></p>
                        </li>
                        <li>
                            <p><i class="fa fa-phone"></i> <strong>Phone:</strong> <?= $footer->footTlp ?></p>
                        </li>
                        <li>
                            <p><i class="fa fa-envelope"></i> <strong>Email:</strong> <a href="mailto:<?= $footer->footEmail ?>"><?= $footer->footEmail ?></a></p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4">
                <h4>Follow Us</h4>
                <ul class="social-icons">
                    <li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                    <li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                    <li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <p>© Copyright 2021. Program Studi Doktoral Ilmu Pertanian | Fakultas Pertanian Universitas Mulawarman.</p>
                </div>
            </div>
        </div>
    </div>
</footer>