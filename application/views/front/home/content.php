<div role="main" class="main">
    <div class="slider-container rev_slider_wrapper">
        <div id="revolutionSlider" class="slider rev_slider" data-plugin-revolution-slider data-plugin-options="{'delay': 9000, 'gridwidth': 1170, 'gridheight': 600, 'disableProgressBar': 'on', 'responsiveLevels': [4096,1200,992,500]}">
            <ul>
                <?php if (!empty($slider)) {
                    foreach ($slider as $row) : ?>
                <li data-transition="fade">
                    <img src="<?php echo base_url('f_home/loadslider/') . $row->sliderFiles ?>" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                </li>
                <?php endforeach;
                } ?>
            </ul>
        </div>
    </div>

    <section class="section mt-0 section-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2><strong>Berita</strong> dan <strong>Artikel</strong> Terbaru</h2>
                </div>
            </div>
            <hr>
            <div class="row mt-4">
                <?php if (!empty($data)) {
                    foreach ($data as $row) : ?>
                        <div class="col-lg-3">
                            <img class="img-fluid img-thumbnail" src="<?php echo base_url('f_home/loadthumb/') . $row->beritaBanner ?>" alt="Blog">
                            <div class="recent-posts mt-3 mb-4">
                                <article class="post">
                                    <h4><a class="text-dark" href="<?php echo base_url() ?>berita/post/<?= $row->beritaNama ?>"><b><?= $row->beritaJudul ?></b></a></h4>
                                    <p><?= substr(strip_tags($row->beritaContent), 0, 180) . '...'; ?></p>
                                    <div class="post-meta">
                                        <span><i class="fa fa-calendar"></i> <?= $row->beritaDatetime ?></span>
                                        <span><i class="fa fa-user"></i> By <a href="#"><?= $row->beritaAuthor ?></a> </span>
                                    </div>
                                </article>
                            </div>
                        </div>
                <?php endforeach;
                } ?>
            </div>
        </div>
    </section>

    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-6 p-0">
                <section class="section section-secondary h-100 m-0">
                    <div class="row justify-content-end m-0">
                        <div class="col-half-section col-half-section-right">
                            <h2 class="heading-dark"><strong>Pengumuman</strong></h2>
                            <div class="row">
                                <?php if (!empty($pengumuman)) {
                                    foreach ($pengumuman as $row) : ?>
                                        <div class="col-sm-6">
                                            <div class="recent-posts">
                                                <article class="post">
                                                    <div class="date">
                                                        <span class="day"><?= date('d', strtotime($row->beritaDatetime)) ?></span>
                                                        <span class="month"><?= date('M', strtotime($row->beritaDatetime)) ?></span>
                                                    </div>
                                                    <h4 class="heading-primary"><a href="<?php echo base_url() ?>pengumuman/post/<?= $row->beritaNama ?>"><?= $row->beritaJudul ?></a></h4>
                                                </article>
                                            </div>
                                        </div>
                                <?php endforeach;
                                } ?>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <div class="col-lg-6 p-0">
                <section class="section section-tertiary h-100 m-0">
                    <div class="row m-0">
                        <div class="col-half-section">
                            <h2 class="heading-dark"><strong>Agenda</strong></h2>
                            <div class="row">
                                <?php if (!empty($agenda)) {
                                    foreach ($agenda as $row) : ?>
                                        <div class="col-sm-6">
                                            <div class="recent-posts">
                                                <article class="post">
                                                    <div class="date">
                                                        <span class="day"><?= date('d', strtotime($row->beritaDatetime)) ?></span>
                                                        <span class="month"><?= date('M', strtotime($row->beritaDatetime)) ?></span>
                                                    </div>
                                                    <h4 class="heading-primary"><a href="<?php echo base_url() ?>agenda/post/<?= $row->beritaNama ?>"><?= $row->beritaJudul ?></a></h4>
                                                </article>
                                            </div>
                                        </div>
                                <?php endforeach;
                                } ?>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

        </div>
    </div>

    <div class="container">
			<div class="row text-center">
				<div class="col">
					<h2 class="mb-0 word-rotator-title mt-4">
						<strong>Link Terkait</strong>
					</h2>
				</div>
			</div>
			<div class="row text-center mt-5">
				<div class="owl-carousel owl-theme" data-plugin-options="{'items': 5, 'autoplay': true, 'autoplayTimeout': 3000}">
					<div>
						<a href="https://unmul.ac.id/" target="blank">
							<img class="img-fluid" src="<?php echo base_url(); ?>front/img/mitra/logo-unmul-a.png" alt="">
						</a>						
					</div>
					<div>
						<a href="https://sia.unmul.ac.id/" target="blank">
							<img class="img-fluid" src="<?php echo base_url(); ?>front/img/mitra/logo-unmul-sia.png" alt="">
						</a>
					</div>
					<div>
						<a href="https://pmb.unmul.ac.id/" target="blank">
							<img class="img-fluid" src="<?php echo base_url(); ?>front/img/mitra/logo-unmul-pmb.png" alt="">
						</a>
					</div>
					<div>
						<a href="https://wisuda.unmul.ac.id/" target="blank">
							<img class="img-fluid" src="<?php echo base_url(); ?>front/img/mitra/logo-unmul-wisuda.png" alt="">
						</a>
					</div>
					<div>
						<a href="https://perkasa.unmul.ac.id/" target="blank">
							<img class="img-fluid" src="<?php echo base_url(); ?>front/img/mitra/forperkasa1.png" alt="">
						</a>
					</div>
				</div>
			</div>
		</div>

	</div>

    <section class="section m-0">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="mb-1"><strong>Pengelola</strong></h2>
                </div>
            </div>
            <hr>
            <div class="row mt-4">
                <div class="col-lg-2 col-6 text-center mb-4">
                    <img src="<?php echo base_url(); ?>front/img/pengelola/drnatan.jpg" class="img-fluid" alt="">
                    <p class="mt-2 mb-0"><strong>Dr.dr. Nataniel Tandirogang, M.Si.</strong></p>
                    <p class="mb-0">Koordinator Program Studi Magister Ilmu Lingkungan</p>
                </div>
                <div class="col-lg-2 col-6 text-center mb-4">
                    <img src="<?php echo base_url(); ?>front/img/pengelola/dewi.jpg" class="img-fluid" alt="">
                    <p class="mt-2 mb-0"><strong>Dewi Rina Mariana, SE</strong></p>
                    <p class="mb-0">Akademik dan Kemahasiswaan</p>
                </div>
                <div class="col-lg-2 col-6 text-center mb-4">
                    <img src="<?php echo base_url(); ?>front/img/pengelola/rahmat.jpg" class="img-fluid" alt="">
                    <p class="mt-2 mb-0"><strong>Rahmad Tulloh, ST.</strong></p>
                    <p class="mb-0">Perlengkapan dan RT</p>
                </div>
                <div class="col-lg-2 col-6 text-center mb-4">
                    <img src="<?php echo base_url(); ?>front/img/pengelola/lidya.jpg" class="img-fluid" alt="">
                    <p class="mt-2 mb-0"><strong>Norlidya Winati, SP.</strong></p>
                    <p class="mb-0">Umum dan Perpustakaan S2 Ilmu Lingkungan</p>
                </div>
                <div class="col-lg-2 col-6 text-center mb-4">
                    <img src="<?php echo base_url(); ?>front/img/pengelola/yuda.jpg" class="img-fluid" alt="">
                    <p class="mt-2 mb-0"><strong>Yuda Yudistira</strong></p>
                    <p class="mb-0">Operator SIA</p>
                </div>
                <div class="col-lg-2 col-6 text-center mb-4">
                    <img src="<?php echo base_url(); ?>front/img/pengelola/fajri.jpg" class="img-fluid" alt="">
                    <p class="mt-2 mb-0"><strong>Fajriyanor</strong></p>
                    <p class="mb-0">Pembantu Umum</p>
                </div>
            </div>
        </div>
    </section>

    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="mb-1"><strong>Galeri</strong></h2>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col">
                <div class="row image-gallery lightbox" data-sort-id="portfolio" data-plugin-options="{'delegate': 'a.lightbox-portfolio', 'type': 'image', 'gallery': {'enabled': true}}">
                    <?php if (!empty($galeri)) {
                        foreach ($galeri as $row) : ?>
                        <div class="col-sm-6 col-lg-3 isotope-item mb-4 mb-lg-0">
                            <div class="image-gallery-item">
                                <a href="<?=base_url('f_home/loadgaleri/').$row->galfFiles?>" class="lightbox-portfolio">
                                    <span class="thumb-info">
                                        <span class="thumb-info-wrapper">
                                            <img src="<?=base_url('f_home/loadgaleri/').$row->galfFiles?>" class="img-fluid" alt="">
                                            <span class="thumb-info-action">
                                                <span class="thumb-info-action-icon"><i class="fa fa-arrows-alt"></i></span>
                                            </span>
                                        </span>
                                    </span>
                                </a>
                            </div>
                        </div>
                        <?php endforeach;
                    } ?>
                </div>
            </div>
        </div>

    </div>

</div>