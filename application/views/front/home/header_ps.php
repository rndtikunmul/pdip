<header id="header" class="header-no-border-bottom has-nav-bar" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 140, 'stickySetTop': '-90px', 'stickyChangeLogo': true}">
	<div class="header-body">
		<div class="header-container container">
			<div class="header-row">
				<div class="header-column">
					<div class="header-row">
						<div class="header-logo">
							<a href="<?php echo base_url(); ?>">
								<img alt="Porto" width="48" height="48" data-sticky-width="250" data-sticky-height="35" data-sticky-top="30" src="<?php echo base_url(); ?>front/img/logo/logo-unmul48.png">
							</a>
						</div>
						<h5 style="color: #fff"><br><?= $header->hdLogo ?><br>Universitas Mulawarman</h5>
					</div>
				</div>
				<div class="header-column justify-content-end">
					<div class="header-row pt-2 pb-3">
						<div class="header-search d-none d-md-block">
							<form id="searchForm" action="<?php echo base_url(); ?>#" method="get">
								<div class="input-group">
									<input type="text" class="form-control" name="q" id="q" placeholder="Search..." required>
									<span class="input-group-btn">
										<button class="btn btn-light" type="submit"><i class="fa fa-search"></i></button>
									</span>
								</div>
							</form>
						</div>
					</div>
					<div class="header-row">
						<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
							<i class="fa fa-bars"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
		<div class="header-nav-bar">
			<div class="header-container container">
				<div class="header-row">
					<div class="header-column">
						<div class="header-row">
							<div class="header-nav justify-content-start">
								<div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1">
									<nav class="collapse">
										<ul class="nav nav-pills" id="mainNav">
											<li class="dropdown">
												<a class="nav-item <?php echo $is_active == 'home' ? 'start active open' : ''; ?>" href="<?php echo base_url(); ?>">
													Beranda
												</a>
											</li>
											<li class="dropdown">
												<a class="dropdown-item dropdown-toggle <?php echo $is_active == 'prodi' ? ' active open' : ''; ?>" href="#">
													Program Studi
												</a>
												<ul class="dropdown-menu">
													<?php if (!empty($prodi)) {
														foreach ($prodi as $row) : ?>
															<li><a class="dropdown-item" href="<?= base_url(); ?>prodi/post/<?= $row->prodiId ?>"><?= $row->prodiNama ?></a></li>
													<?php endforeach;
													} ?>
												</ul>
											</li>
											<li class="dropdown">
												<a class="dropdown-item dropdown-toggle <?php echo $is_active == 'lab' ? ' active open' : ''; ?>" href="#">
													Laboratorium
												</a>
												<ul class="dropdown-menu">
													<?php if (!empty($lab)) {
														foreach ($lab as $row) : ?>
															<li><a class="dropdown-item" href="<?= base_url(); ?>lab/post/<?= $row->labId ?>"><?= $row->labNama ?></a></li>
													<?php endforeach;
													} ?>
												</ul>
											</li>
											<?php
											//print_r($menu);
											foreach ($menu as $row) {
												//echo $is_active;
											?>
												<li class="dropdown">
													<a class="dropdown-item dropdown-toggle <?= $row['headId'] == $is_active ? 'active open' : '' ?>" href="#">
														<?= $row['headNama'] ?>
													</a>
													<ul class="dropdown-menu">
														<?php
														foreach ($row['child'] as $cRow) {
														?>
															<li><a class="dropdown-item" href="<?= base_url(); ?>page/post/<?= $cRow->pageNama ?>"><?= $cRow->pageJudul ?></a></li>
														<?php
														}
														?>
													</ul>
												</li>
											<?php
											}
											?>
											
											<!-- <li class="dropdown">
												<a class="dropdown-item dropdown-toggle <?php echo $is_active == 'jurnal' ? ' active open' : ''; ?>" href="#">
													Jurnal
												</a>
												<ul class="dropdown-menu">
													<?php if (!empty($jurnam)) {
														foreach ($jurnam as $row) : ?>
															<li><a class="dropdown-item" href="<?= base_url(); ?>jurnam/post/<?= $row->jurnamId ?>"><?= $row->jurnamNama ?></a></li>
													<?php endforeach;
													} ?>
												</ul>
											</li> -->
											<li class="dropdown">
												<a class="dropdown-item dropdown-toggle <?php echo $is_active == 'ragam' ? ' active open' : ''; ?>" href="#">
													Ragam Informasi
												</a>
												<ul class="dropdown-menu">
													<li><a class="dropdown-item" href="<?= base_url(); ?>pengumuman/">Pengumuman</a></li>
													<li><a class="dropdown-item" href="<?= base_url(); ?>berita/">Berita & Kegiatan</a></li>
													<li><a class="dropdown-item" href="<?= base_url(); ?>agenda/">Agenda</a></li>
												</ul>
											</li>
										</ul>
									</nav>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>