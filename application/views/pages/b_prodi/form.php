<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="prodiIdOld" value="<?= $datas != false ? $datas->prodiId : '' ?>">

                        <div class="form-group">
                            <label>Jurusan</label>
                            <select class="form-control m-select2" name="prodijurusanId">
                                <option value=""></option>
                                <?php
                                foreach ($f_jurusan as $row) :
                                    echo '<option value="' . $row->jurusanId . '" ' . ($datas != false ? $row->jurusanId == $datas->prodijurusanId ? 'selected' : '' : '') . '>' . $row->jurusanNama . '</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>Program Studi</label>
                            <input type="text" class="form-control" name="prodiNama" placeholder="nama program studi" aria-describedby="prodiNama" value="<?= $datas != false ? $datas->prodiNama : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Deskripsi</label>
                            <textarea cols="60" class="summernote" rows="10" id="m_summernote_1" name="prodiDesk" placeholder="prodiDesk" aria-describedby="prodiDesk"> <?= $datas != false ? $datas->prodiDesk : '' ?>
                        </textarea>
                        </div>

                        <div class="form-group">
                            <label>Visi</label>
                            <textarea cols="60" class="summernote" rows="10" id="m_summernote_1" name="prodiVisi" placeholder="prodiVisi" aria-describedby="prodiVisi"> <?= $datas != false ? $datas->prodiVisi : '' ?>
                        </textarea>
                        </div>

                        <div class="form-group">
                            <label>Misi</label>
                            <textarea cols="60" class="summernote" rows="10" id="m_summernote_1" name="prodiMisi" placeholder="prodiMisi" aria-describedby="prodiMisi"> <?= $datas != false ? $datas->prodiMisi : '' ?>
                        </textarea>
                        </div>

                        <div class="form-group">
                            <label>Tujuan</label>
                            <textarea cols="60" class="summernote" rows="10" id="m_summernote_1" name="prodiTujuan" placeholder="prodiTujuan" aria-describedby="prodiTujuan"> <?= $datas != false ? $datas->prodiTujuan : '' ?>
                        </textarea>
                        </div>

                        <div class="form-group">
                            <label>Kurikulum</label>
                            <textarea cols="60" class="summernote" rows="10" id="m_summernote_1" name="prodiKurikulum" placeholder="prodiKurikulum" aria-describedby="prodiKurikulum"> <?= $datas != false ? $datas->prodiKurikulum : '' ?>
                        </textarea>
                        </div>

                        <div class="form-group">
                            <label>Kegiatan</label>
                            <textarea cols="60" class="summernote" rows="10" id="m_summernote_1" name="prodiKegiatan" placeholder="prodiKegiatan" aria-describedby="prodiKegiatan"> <?= $datas != false ? $datas->prodiKegiatan : '' ?>
                        </textarea>
                        </div>

                        <div class="form-group">
                            <label>Penelitian</label>
                            <textarea cols="60" class="summernote" rows="10" id="m_summernote_1" name="prodiPenelitian" placeholder="prodiPenelitian" aria-describedby="prodiPenelitian"> <?= $datas != false ? $datas->prodiPenelitian : '' ?>
                        </textarea>
                        </div>

                        <div class="form-group">
                            <label>Pengabdian</label>
                            <textarea cols="60" class="summernote" rows="10" id="m_summernote_1" name="prodiPengabdian" placeholder="prodiPengabdian" aria-describedby="prodiPengabdian"> <?= $datas != false ? $datas->prodiPengabdian : '' ?>
                        </textarea>
                        </div>

                        <div class="form-group">
                            <label>Panduan</label>
                            <textarea cols="60" class="summernote" rows="10" id="m_summernote_1" name="prodiPanduan" placeholder="prodiPanduan" aria-describedby="prodiPanduan"> <?= $datas != false ? $datas->prodiPanduan : '' ?>
                        </textarea>
                        </div>

                        <div class="form-group">
                            <label>Jadwal</label>
                            <textarea cols="60" class="summernote" rows="10" id="m_summernote_1" name="prodiJadwal" placeholder="prodiJadwal" aria-describedby="prodiJadwal"> <?= $datas != false ? $datas->prodiJadwal : '' ?>
                        </textarea>
                        </div>

                        <div class="form-group">
                            <label>Buku</label>
                            <textarea cols="60" class="summernote" rows="10" id="m_summernote_1" name="prodiBuku" placeholder="prodiBuku" aria-describedby="prodiBuku"> <?= $datas != false ? $datas->prodiBuku : '' ?>
                        </textarea>
                        </div>

                        <div class="form-group">
                            <label>Formulir</label>
                            <textarea cols="60" class="summernote" rows="10" id="m_summernote_1" name="prodiFormulir" placeholder="prodiFormulir" aria-describedby="prodiFormulir"> <?= $datas != false ? $datas->prodiFormulir : '' ?>
                        </textarea>
                        </div>

                        <div class="form-group">
                            <label>Link Website</label>
                            <input type="text" class="form-control" name="prodiLink" placeholder="prodiLink" aria-describedby="prodiLink" value="<?= $datas != false ? $datas->prodiLink : '' ?>">
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->