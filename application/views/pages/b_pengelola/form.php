<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_upload" enctype="multipart/form-data">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="dokidOld" value="<?= $datas != false ? $datas->dokid : '' ?>">

                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" class="form-control" name="doknama" placeholder="Nama" aria-describedby="Nama" value="<?= $datas != false ? $datas->doknama : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Bidang</label>
                            <input type="text" class="form-control" name="dokbidang" placeholder="Bidang" aria-describedby="Bidang" value="<?= $datas != false ? $datas->dokbidang : '' ?>">
                        </div>

                        <div class="form-group">
                            <label>Upload Foto</label>
                            <div class="custom-file">
                                <input type="file" id="custom-file" class="custom-file-input" name="dokfoto" placeholder="Foto" aria-describedby="Foto" value="<?= $datas != false ? $datas->dokfoto : '' ?>">
                                <label class="custom-file-label" for="customFile">Choose file</label>
                                <span class="m-form__help">
                                    Filetipe: *.jpg; *.jpeg; *.png | foto optimal - skala 1:1 | resolusi min 240p, max 480p
                                </span>
                            </div>
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->