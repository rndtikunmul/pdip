
<?php
class Model_b_files extends Model_Master
{
    protected $table = 'f_files';

    public function __construct()
    {
        parent::__construct();
    }

    function all($user)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('fileUserId', $user);
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }
}
