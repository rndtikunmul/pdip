
<?php
class Model_b_slider extends Model_Master
{
    protected $table = 'f_slider';

    public function __construct()
    {
        parent::__construct();
    }       
    
    function get_slider($number, $start = 0)
	{
		$this->db->select('*');
		$this->db->from('f_slider');
		$this->db->order_by('sliderId', 'desc');
        $this->db->limit($number, $start);
		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return FALSE;
	}
}
            