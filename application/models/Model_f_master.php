<?php
class Model_f_master extends Model_Master
{
    public function __construct()
    {
        parent::__construct();
    }

    function get_prodi()
	{
		$this->db->select('*');
        $this->db->from('f_jurusan');
        $this->db->join('f_prodi','jurusanId = prodijurusanId','LEFT');
		$this->db->order_by('jurusanId,prodiId');
		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return FALSE;
	}

	function get_lab()
	{
		$this->db->select('*');
        $this->db->from('f_lab');
		$this->db->order_by('labId');
		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return FALSE;
	}

	function get_jurnam()
	{
		$this->db->select('*');
        $this->db->from('f_jurnal_nama');
		$this->db->order_by('jurnamId');
		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return FALSE;
	}
}