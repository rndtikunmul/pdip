
<?php
class Model_b_galfoto extends Model_Master
{
    protected $table = 'f_galeri_foto';


    public function __construct()
    {
        parent::__construct();
    }
    function all()
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('f_head_galeri_foto', 'galfHeadgalfId = headgalfId', 'LEFT');
        $qr = $this->db->get();
        if ($qr->num_rows() > 0)
            return $qr->result();
        else
            return false;
    }

    function by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('f_head_galeri_foto', 'galfHeadgalfId = headgalfId', 'LEFT');
        $this->db->where($id);
        $qr = $this->db->get();
        if ($qr->num_rows() == 1)
            return $qr->row();
        else
            return false;
    }

    function get_foto_last($number, $start = 0)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('f_head_galeri_foto', 'galfHeadgalfId = headgalfId', 'LEFT');
        $this->db->order_by('galfDatetime', 'desc');        
        $this->db->limit($number, $start);
        $qr = $this->db->get();
        if ($qr->num_rows() > 0)
            return $qr->result();
        else
            return false;
    }
}
