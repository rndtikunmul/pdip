<?php
class Model_berita extends CI_Model
{
	function get($number =500, $start = 0)
	{
		$this->db->select('*');
		$this->db->from('f_berita');
		$this->db->order_by('beritaDatetime', 'desc');
		$this->db->limit($number, $start);		
		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return FALSE;
	}

	function get_berita($section)
	{
		$this->db->select('*');
		$this->db->from('f_berita');
		$this->db->where('beritaSection', $section);
		$this->db->order_by('beritaDatetime', 'desc');
        // $this->db->limit($number, $start);
		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return FALSE;
	}
	
	function get_nama($key)
	{
		$this->db->select('*');
		$this->db->from('f_berita');
		$this->db->join('s_user','susrNama=beritaAuthor','left');
		$this->db->where('beritaNama',$key);
		$qr=$this->db->get();

		if($qr->num_rows()==1)
			return $qr->row();
		else
			return FALSE;
	}

	function cari_berita($keyword)
	{
		$matchb = $this->input->post('cari');
		$this->db->select('*');
		$this->db->from('f_berita');
		$this->db->join('s_user','susrNama=beritaAuthor','left');
		$this->db->like('beritaNama',$keyword);
		// $this->db->or_like('beritaContent',$keycari);
		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return FALSE;
	}

}
?>