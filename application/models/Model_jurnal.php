<?php
class Model_jurnal extends CI_Model
{	
	function get_id($key)
	{
		$this->db->select('*');
		$this->db->from('f_jurnal');
		$this->db->where('jurnalId',$key);
		$qr=$this->db->get();

		if($qr->num_rows()==1)
			return $qr->row();
		else
			return FALSE;
	}

	function get($key)
    {
        $this->db->select('*');
        $this->db->from('f_jurnal');
		$this->db->join('f_jurnal_nama','jurnamId=jurnalnamaId', 'LEFT');
		$this->db->where('jurnalnamaId',$key);
        $qr = $this->db->get();
        if ($qr->num_rows() > 0)
            return $qr->result();
        else
            return false;
    }

	function get_lab_id($key)
	{
		$this->db->select('*');
		$this->db->from('f_lab');
		$this->db->join('f_prodi','prodiId=labprodiId','left');
		$this->db->where('labId',$key);
		$qr=$this->db->get();

		if($qr->num_rows()==1)
			return $qr->row();
		else
			return FALSE;
	}

	function cari_berita($keyword)
	{
		$matchb = $this->input->post('cari');
		$this->db->select('*');
		$this->db->from('f_berita');
		$this->db->join('s_user','susrNama=beritaAuthor','left');
		$this->db->like('beritaNama',$keyword);
		// $this->db->or_like('beritaContent',$keycari);
		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return FALSE;
	}

}
?>