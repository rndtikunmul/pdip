
<?php
class Model_b_page extends Model_Master
{
        protected $table = 'f_page';


    public function __construct()
    {
        parent::__construct();
    }       
    
    function all($user)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('s_user_modul_group_ref','pageHead = susrmdgroupNama','LEFT');
        $this->db->join('s_user','susrNama=pageAuthor', 'LEFT');
        $this->db->where("pageAuthor = '" . $user . "' OR susrSgroupNama = ", 'ADMIN');
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }

    function by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('s_user_modul_group_ref','pageHead = susrmdgroupNama','LEFT');
        $this->db->where($id);
        $qr=$this->db->get();
        if($qr->num_rows()==1)
            return $qr->row();
        else
            return false;
    }

 }
            