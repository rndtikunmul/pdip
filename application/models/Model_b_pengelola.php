
<?php
class Model_b_pengelola extends Model_Master
{
    protected $table = 'ref_pengelola';

    public function __construct()
    {
        parent::__construct();
    }

    function get_pengelola()
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->order_by('kelurut');
        $qr = $this->db->get();
        if ($qr->num_rows() > 0)
            return $qr->result();
        else
            return false;
    }
}
