<?php
class Model_galeri extends CI_Model
{
	public function get_data()
	{
		return $this->db->get('f_galeri_foto');
	}

	function get_folder($number = 100, $start = 0)
	{
		$this->db->select('*');
		$this->db->from('f_head_galeri_foto');
		$this->db->order_by('headgalfId', 'desc');
		$this->db->limit($number, $start);
		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return FALSE;
	}

	function get_foto($number = 100, $start = 0)
	{
		$this->db->select('*');
		$this->db->from('f_galeri_foto');
		$this->db->join('f_head_galeri_foto','headgalfId=galfHeadgalfId','left');
		$this->db->order_by('headgalfId', 'desc');		
        $this->db->limit($number, $start);
		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return FALSE;
	}

	function get_foto_last($number = 100, $start = 0)
	{
		$this->db->select('*');
		$this->db->from('f_galeri_foto');
		$this->db->join('f_head_galeri_foto','headgalfId=galfHeadgalfId','left');
		$this->db->order_by('galfDatetime', 'desc');		
        $this->db->limit($number, $start);
		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return FALSE;
	}

	function get_foto_by_id($key)
	{
		$this->db->select('*');
		$this->db->from('f_galeri_foto');
		$this->db->where('galfId',$key);
		$this->db->order_by('galfId','desc');
		$this->db->limit(5);
		$qr=$this->db->get();

		if($qr->num_rows()==1)
			return $qr->row();
		else
			return FALSE;
	}

	function get_video($number = 2, $start = 0)
	{
		$this->db->select('*');
		$this->db->from('f_galeri_video');
		$this->db->order_by('galvDatetime', 'desc');		
        $this->db->limit($number, $start);
		$qr=$this->db->get();

		if($qr->num_rows()>0)
			return $qr->result();
		else
			return FALSE;
	}

}
?>