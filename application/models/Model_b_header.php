
<?php
class Model_b_header extends Model_Master
{


    public function __construct()
    {
        parent::__construct();
    }

    function get($hdId = 1)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where($hdId);
        $qr=$this->db->get();
        if($qr->num_rows()==1)
            return $qr->row();
        else
            return false;
    }
}
