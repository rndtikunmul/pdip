
<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class b_statistik extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/b_statistik/';
        $this->_path_js = null;
        $this->_judul = 'Statistik Fakultas';
        $this->_controller_name = 'b_statistik';
        $this->_model_name = 'model_b_statistik';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->get_ref_table('f_statistik');
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['statId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->get_by_id('f_statistik', $key);

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $statIdOld = $this->input->post('statIdOld');
        $this->form_validation->set_rules('statKerjasama', 'statKerjasama', 'trim|xss_clean');
        $this->form_validation->set_rules('statProdi', 'statProdi', 'trim|xss_clean');
        $this->form_validation->set_rules('statMhs', 'statMhs', 'trim|xss_clean');
        $this->form_validation->set_rules('statDosen', 'statDosen', 'trim|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $statKerjasama = $this->input->post('statKerjasama');
                $statProdi = $this->input->post('statProdi');
                $statMhs = $this->input->post('statMhs');
                $statDosen = $this->input->post('statDosen');


                $param = array(
                    'statKerjasama' => $statKerjasama,
                    'statProdi' => $statProdi,
                    'statMhs' => $statMhs,
                    'statDosen' => $statDosen,

                );

                if (empty($statIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('f_statistik', $param);
                } else {
                    $key = array('statId' => $statIdOld);
                    $proses = $this->{$this->_model_name}->update('f_statistik', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['statId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('f_statistik', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }
}
