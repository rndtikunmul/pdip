<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class b_berita extends MY_Controller {
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/b_berita/';
        $this->_path_js = 'b_berita/';
        $this->_judul = 'Ragam Informasi';
        $this->_controller_name = 'b_berita';
        $this->_model_name = 'model_b_berita';
        $this->_page_index = 'index';
        $this->_path_upload = '../upload_file/berita/';
        $this->_logged_in = $this->session->userdata('logged_in');

        $this->load->model($this->_model_name,'',TRUE);
    }

    public function index()
    {
        $session_data = $this->session->userdata('logged_in');
        $user = $this->_logged_in['susrNama'];
        $data = $this->get_master($this->_path_page.$this->_page_index);
        $data['scripts'] = [$this->_path_js . 'b_berita'];
        $data['datas'] = $this->{$this->_model_name}->by_user($user);
        // print_r($this->db->last_query());
        // exit();
        $data['create_url'] = site_url($this->_controller_name.'/create').'/';
        $data['update_url'] = site_url($this->_controller_name.'/update').'/';
        $data['delete_url'] = site_url($this->_controller_name.'/delete').'/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {	
        $data = $this->get_master($this->_path_page.'form');	
        $data['scripts'] = [$this->_path_js . 'b_berita'];	
        $data['save_url'] = site_url($this->_controller_name.'/save').'/';	
        $data['status_page'] = 'Create';
        $data['datas'] = false;
            
        $this->load->view($this->_template, $data);
    }

    public function update()
    {		
        $data = $this->get_master($this->_path_page.'form');	
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $data['scripts'] = [$this->_path_js . 'b_berita'];	
        $data['save_url'] = site_url($this->_controller_name.'/save').'/';	
        $data['status_page'] = 'Update';
        $key = ['beritaId'=>$keyS];
        $data['datas'] = $this->{$this->_model_name}->get_by_id('f_berita',$key);
            
        $this->load->view($this->_template, $data);
    }

    public function save()
    {	
        $session_data = $this->session->userdata('logged_in');	
        $beritaIdOld = $this->input->post('beritaIdOld');
        $this->form_validation->set_rules('beritaSection','beritaSection','trim|xss_clean');
        $this->form_validation->set_rules('beritaJudul','beritaJudul','trim|xss_clean|required');
        $this->form_validation->set_rules('beritaContent','beritaContent','trim|xss_clean|required');
        $this->form_validation->set_rules('beritaDatetime','beritaDatetime','trim|xss_clean|required');
        $this->form_validation->set_rules('beritaTag','beritaTag','trim|xss_clean|required');

        if($this->form_validation->run()) 
        {	
            if(IS_AJAX)
            {
                $beritaSection = $this->input->post('beritaSection');
                $beritaJudul = $this->input->post('beritaJudul');
                $beritaContent = $this->input->post('beritaContent');
                $beritaDatetime = $this->input->post('beritaDatetime');
                $beritaTag = $this->input->post('beritaTag');
                $beritaAuthor = $this->_logged_in['susrNama'];
                $beritaBanner = $this->input->post('berkas');
                $beritaNama = str_replace(' ','.',(preg_replace('/[^a-z0-9]+/i', ' ', strip_tags($this->input->post('beritaJudul')))));

                $param = array(
                    'beritaNama'=>$beritaNama,
                    'beritaSection'=>$beritaSection,
                    'beritaJudul'=>$beritaJudul,
                    'beritaContent'=>$beritaContent,
                    'beritaDatetime'=>date("Y-m-d",strtotime($beritaDatetime)),
                    'beritaTag'=>$beritaTag,
                    'beritaAuthor'=>$beritaAuthor

                );

                if (!empty($_FILES['berkas']['name'])) {
                    $config['upload_path'] = realpath(APPPATH . '../upload_file/berita/'); //path folder
                    $config['allowed_types'] = 'jpg|jpeg'; //type yang dapat diakses bisa anda sesuaikan
                    $config['max_size'] = 1024 * 5; //maksimum besar file 5M
                    $config['file_name'] = $beritaSection. '_' .mt_rand(); //nama yang terupload nantinya

                    // print_r($config);
                    // exit();

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('berkas'))
                        echo message(strip_tags($this->upload->display_errors()), 'error');
                    else {
                        $file = $this->upload->data();
                        $image_path = $file['full_path'];

                        // echo "<pre>";
                        // print_r($image_path);
                        // echo "</pre>";
                        // exit;
                        $param['beritaBanner'] = $file['file_name'];
                        if (file_exists($image_path)) {
                            $upload = true;
                            $this->load->helper('image_thumb');
                            image_thumb($config['upload_path'], $param['beritaBanner'], 400, 300);
                        } else {
                            $upload = false;
                        }
                    }
                }

                if(empty($beritaIdOld))
                {
                    $proses = $this->{$this->_model_name}->insert('f_berita',$param);
                } else {
                    $key = array('beritaId'=>$beritaIdOld);
                    $proses = $this->{$this->_model_name}->update('f_berita',$param,$key);
                }

                if($proses)
                    message($this->_judul.' Berhasil Disimpan','success');
                else
                {
                    $error = $this->db->error();
                    message($this->_judul.' Gagal Disimpan, '.$error['code'].': '.$error['message'],'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! '.validation_errors(),'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $key = ['beritaId'=>$keyS];
        $proses = $this->{$this->_model_name}->delete('f_berita',$key);
        if ($proses) 
            message($this->_judul.' Berhasil Dihapus','success');
        else
        {
            $error = $this->db->error();
            message($this->_judul.' Gagal Dihapus, '.$error['code'].': '.$error['message'],'error');
        }
    }

    public function loadimage()
    {
        $file = $this->uri->segment(3);
        ob_clean();
        $path = FCPATH . '../upload_file/berita/'. $file;
        $size = getimagesize($path);
        header('Content-Type:' . $size['mime']);
        switch ($size['mime']) {
            case 'image/png':
            $img = imagecreatefrompng($path);

            imagepng($img);
            break;

            default:
            $img = imagecreatefromjpeg($path);
            imagejpeg($img);
            break;
        }
        imagedestroy($img);
    }
}
