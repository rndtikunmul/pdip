
<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class b_prodi extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/b_prodi/';
        $this->_path_js = null;
        $this->_judul = 'Program Studi';
        $this->_controller_name = 'b_prodi';
        $this->_model_name = 'model_b_prodi';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->all();
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['f_jurusan'] = $this->{$this->_model_name}->get_ref_table('f_jurusan');

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['prodiId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        $data['f_jurusan'] = $this->{$this->_model_name}->get_ref_table('f_jurusan');

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $session_data = $this->session->userdata('logged_in');
        $prodiIdOld = $this->input->post('prodiIdOld');
        $this->form_validation->set_rules('prodijurusanId', 'prodijurusanId', 'trim|xss_clean');
        $this->form_validation->set_rules('prodiNama', 'prodiNama', 'trim|xss_clean');
        $this->form_validation->set_rules('prodiDesk', 'prodiDesk', 'trim|xss_clean');
        $this->form_validation->set_rules('prodiVisi', 'prodiVisi', 'trim|xss_clean');
        $this->form_validation->set_rules('prodiMisi', 'prodiMisi', 'trim|xss_clean');
        $this->form_validation->set_rules('prodiTujuan', 'prodiTujuan', 'trim|xss_clean');
        $this->form_validation->set_rules('prodiKurikulum', 'prodiKurikulum', 'trim|xss_clean');
        $this->form_validation->set_rules('prodiKegiatan', 'prodiKegiatan', 'trim|xss_clean');
        $this->form_validation->set_rules('prodiPenelitian', 'prodiPenelitian', 'trim|xss_clean');
        $this->form_validation->set_rules('prodiPengabdian', 'prodiPengabdian', 'trim|xss_clean');
        $this->form_validation->set_rules('prodiPanduan', 'prodiPanduan', 'trim|xss_clean');
        $this->form_validation->set_rules('prodiJadwal', 'prodiJadwal', 'trim|xss_clean');
        $this->form_validation->set_rules('prodiBuku', 'prodiBuku', 'trim|xss_clean');
        $this->form_validation->set_rules('prodiFormulir', 'prodiFormulir', 'trim|xss_clean');
        $this->form_validation->set_rules('prodiLink', 'prodiLink', 'trim|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $prodijurusanId = $this->input->post('prodijurusanId');
                $prodiNama = $this->input->post('prodiNama');
                $prodiDesk = $this->input->post('prodiDesk');
                $prodiVisi = $this->input->post('prodiVisi');
                $prodiMisi = $this->input->post('prodiMisi');
                $prodiTujuan = $this->input->post('prodiTujuan');
                $prodiKurikulum = $this->input->post('prodiKurikulum');
                $prodiKegiatan = $this->input->post('prodiKegiatan');
                $prodiPenelitian = $this->input->post('prodiPenelitian');
                $prodiPengabdian = $this->input->post('prodiPengabdian');
                $prodiPanduan = $this->input->post('prodiPanduan');
                $prodiJadwal = $this->input->post('prodiJadwal');
                $prodiBuku = $this->input->post('prodiBuku');
                $prodiFormulir = $this->input->post('prodiFormulir');
                $prodiLink = $this->input->post('prodiLink');


                $param = array(
                    'prodijurusanId' => $prodijurusanId,
                    'prodiNama' => $prodiNama,
                    'prodiDesk' => $prodiDesk,
                    'prodiVisi' => $prodiVisi,
                    'prodiMisi' => $prodiMisi,
                    'prodiTujuan' => $prodiTujuan,
                    'prodiKurikulum' => $prodiKurikulum,
                    'prodiKegiatan' => $prodiKegiatan,
                    'prodiPenelitian' => $prodiPenelitian,
                    'prodiPengabdian' => $prodiPengabdian,
                    'prodiPanduan' => $prodiPanduan,
                    'prodiJadwal' => $prodiJadwal,
                    'prodiBuku' => $prodiBuku,
                    'prodiFormulir' => $prodiFormulir,
                    'prodiLink' => $prodiLink,

                );

                if (empty($prodiIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('f_prodi', $param);
                } else {
                    $key = array('prodiId' => $prodiIdOld);
                    $proses = $this->{$this->_model_name}->update('f_prodi', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['prodiId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('f_prodi', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }
}
