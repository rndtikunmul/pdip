<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class f_home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
		$this->_controller_name = 'f_home';
		$this->_model = 'model_f_master';
		$this->_path_js = 'home';
		$this->load->model('model_berita','',TRUE);
		$this->load->model('model_b_berita','',TRUE);
		$this->load->model('model_b_slider','',TRUE);
		$this->load->model($this->_model, '', TRUE);
		$this->load->model('model_b_galfoto','',TRUE);
	}

	public function index()
	{
		$data['is_active'] = 'home';
		$header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
		$footer = $this->{$this->_model}->get_by_id('f_footer',['footId'=>1]);
		$stat = $this->{$this->_model}->get_by_id('f_statistik',['statId'=>1]);
		$data['header'] = $header;
		$data['footer'] = $footer;
		$data['stat'] = $stat;
		$data['data'] = $this->model_berita->get(4);
		$data['slider'] = $this->model_b_slider->get_slider(3);
		// print_r($footer);
		// exit();		
		$data['pengumuman'] = $this->model_b_berita->get_berita('Pengumuman',4);
		// $data['berita'] = $this->model_b_berita->get_berita('Berita',4);
		// $data['kegiatan'] = $this->model_b_berita->get_berita('Kegiatan',4);
		$data['agenda'] = $this->model_b_berita->get_berita('Agenda',4);
		$data['menu'] = menu();
		$data['galeri'] = $this->model_b_galfoto->get_foto_last(4);
		$this->load->view('front/home/template', $data);
	}

	public function loadimage()
	{
		$file = $this->uri->segment(3);
		ob_clean();
		$path = FCPATH . '../upload_file/berita/'. $file;
		$size = getimagesize($path);
		header('Content-Type:' . $size['mime']);
		switch ($size['mime']) {
			case 'image/png':
			$img = imagecreatefrompng($path);

			imagepng($img);
			break;

			default:
			$img = imagecreatefromjpeg($path);
			imagejpeg($img);
			break;
		}
		imagedestroy($img);
	}

	public function loadthumb()
	{
		$file = $this->uri->segment(3);
		ob_clean();
		$path = FCPATH . '../upload_file/berita/thumb/'. $file;
		$size = getimagesize($path);
		header('Content-Type:' . $size['mime']);
		switch ($size['mime']) {
			case 'image/png':
			$img = imagecreatefrompng($path);

			imagepng($img);
			break;

			default:
			$img = imagecreatefromjpeg($path);
			imagejpeg($img);
			break;
		}
		imagedestroy($img);
	}

	public function loadgaleri()
	{
		$file = $this->uri->segment(3);
		ob_clean();
		$path = FCPATH . '../upload_file/galeri/'. $file;
		$size = getimagesize($path);
		header('Content-Type:' . $size['mime']);
		switch ($size['mime']) {
			case 'image/png':
			$img = imagecreatefrompng($path);

			imagepng($img);
			break;

			default:
			$img = imagecreatefromjpeg($path);
			imagejpeg($img);
			break;
		}
		imagedestroy($img);
	}

	public function loadslider()
	{
		$file = $this->uri->segment(3);
		ob_clean();
		$path = FCPATH . '../upload_file/files/'. $file;
		$size = getimagesize($path);
		header('Content-Type:' . $size['mime']);
		switch ($size['mime']) {
			case 'image/png':
			$img = imagecreatefrompng($path);

			imagepng($img);
			break;

			default:
			$img = imagecreatefromjpeg($path);
			imagejpeg($img);
			break;
		}
		imagedestroy($img);
	}
}
