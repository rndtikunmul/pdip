<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class Pengumuman extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->_controller_name = 'pengumuman';
		$this->_model = 'model_f_master';
		$this->load->model('model_berita','',TRUE);
		$this->load->model('model_b_berita','',TRUE);
		$this->load->model($this->_model, '', TRUE);
	}

	public function index()//single post page
    {
    	$data['is_active'] = 'ragam';
        $data['menu'] = menu();
        $data['pages'] = 'page/pengumuman/view';
        $header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
		$data['header'] = $header;
        $data['all'] = $this->model_berita->get(5);
		$data['pengumuman'] = $this->model_berita->get_berita('PENGUMUMAN',5);
        $data['menu'] = menu();
		$this->load->view('page/template', $data);
    }

	public function post($beritaNama)//single post page
    {
    	$data['is_active'] = 'beritapost';
        $data['pages'] = 'page/berita/post';
        $data['data'] = $this->model_berita->get(5);
		$data['datas'] = $this->model_berita->get_nama($beritaNama);
		$data['all'] = $this->model_berita->get(5);
		$header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
		$data['header'] = $header;
        $data['menu'] = menu();
        $this->load->view('page/template', $data);
	}

	public function loadimage()
	{
		$file = $this->uri->segment(3);
		ob_clean();
		$path = FCPATH . '../upload_file/berita/'. $file;
		$size = getimagesize($path);
		header('Content-Type:' . $size['mime']);
		switch ($size['mime']) {
			case 'image/png':
			$img = imagecreatefrompng($path);

			imagepng($img);
			break;

			default:
			$img = imagecreatefromjpeg($path);
			imagejpeg($img);
			break;
		}
		imagedestroy($img);
	}
	
	public function loadthumb()
	{
		$file = $this->uri->segment(3);
		ob_clean();
		$path = FCPATH . '../upload_file/berita/thumb/'. $file;
		$size = getimagesize($path);
		header('Content-Type:' . $size['mime']);
		switch ($size['mime']) {
			case 'image/png':
			$img = imagecreatefrompng($path);

			imagepng($img);
			break;

			default:
			$img = imagecreatefromjpeg($path);
			imagejpeg($img);
			break;
		}
		imagedestroy($img);
	}

}