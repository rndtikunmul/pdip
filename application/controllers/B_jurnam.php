
<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class b_jurnam extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/b_jurnam/';
        $this->_path_js = null;
        $this->_judul = 'Jurnal Nama';
        $this->_controller_name = 'b_jurnam';
        $this->_model_name = 'model_b_jurnam';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->all();
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['f_prodi'] = $this->{$this->_model_name}->get_ref_table('f_prodi');

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['jurnamId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        $data['f_prodi'] = $this->{$this->_model_name}->get_ref_table('f_prodi');

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $jurnamIdOld = $this->input->post('jurnamIdOld');
        $this->form_validation->set_rules('jurnamprodiId', 'jurnamprodiId', 'trim|xss_clean');
        $this->form_validation->set_rules('jurnamNama', 'jurnamNama', 'trim|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $jurnamprodiId = $this->input->post('jurnamprodiId');
                $jurnamNama = $this->input->post('jurnamNama');


                $param = array(
                    'jurnamprodiId' => $jurnamprodiId,
                    'jurnamNama' => $jurnamNama,

                );

                if (empty($jurnamIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('f_jurnal_nama', $param);
                } else {
                    $key = array('jurnamId' => $jurnamIdOld);
                    $proses = $this->{$this->_model_name}->update('f_jurnal_nama', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['jurnamId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('f_jurnal_nama', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }
}
