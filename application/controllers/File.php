<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class File extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->_controller_name = 'file';
	}

	public function loadimage()
	{
		$file = $this->uri->segment(3);
		ob_clean();
		$path = FCPATH . '../upload_file/berita/'. $file;
		$size = getimagesize($path);
		header('Content-Type:' . $size['mime']);
		switch ($size['mime']) {
			case 'image/png':
			$img = imagecreatefrompng($path);

			imagepng($img);
			break;

			default:
			$img = imagecreatefromjpeg($path);
			imagejpeg($img);
			break;
		}
		imagedestroy($img);
	}
	
	public function loadthumb()
	{
		$file = $this->uri->segment(3);
		ob_clean();
		$path = FCPATH . '../upload_file/berita/thumb/'. $file;
		$size = getimagesize($path);
		header('Content-Type:' . $size['mime']);
		switch ($size['mime']) {
			case 'image/png':
			$img = imagecreatefrompng($path);

			imagepng($img);
			break;

			default:
			$img = imagecreatefromjpeg($path);
			imagejpeg($img);
			break;
		}
		imagedestroy($img);
	}

	public function loadattach()
    {
        ob_clean();
        $this->load->helper('file');
        $file = $this->uri->segment(3);
        $path = FCPATH . '../upload_file/files/' . $file;
        $files = get_mime_by_extension($path);
        // echo $files;
        // exit();
        if ($files == 'application/pdf') {
            header('Content-type: application/pdf');
            header('Content-Disposition: inline; filename="' . $path . '"');
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
            @readfile($path);

        }   elseif 
            ($files == 'application/msword') {
            header('Content-type: application/msword');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path);    

        }   elseif 
            ($files == 'application/vnd.ms-excel') {
            header('Content-type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path); 
            
        }   elseif 
            ($files == 'application/vnd.ms-powerpoint') {
            header('Content-type: application/vnd.ms-powerpoint');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path);    

        }   elseif 
            ($files == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
            header('Content-type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path);
            
        }   elseif 
            ($files == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
            header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path);    
    
        }   elseif 
            ($files == 'application/vnd.openxmlformats-officedocument.presentationml.presentation') {
            header('Content-type: application/vnd.openxmlformats-officedocument.presentationml.presentation');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($path);
            
        }   else {
            $size = getimagesize($path);
            header('Content-Type:' . $size['mime']);
            switch ($size['mime']) {
                case 'image/png':
                    $img = imagecreatefrompng($path);

                    imagepng($img);
                    break;

                default:
                    $img = imagecreatefromjpeg($path);
                    imagejpeg($img);
                    break;
            }
            imagedestroy($img);
        }
    }

}