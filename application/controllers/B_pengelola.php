<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class b_pengelola extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/b_pengelola/';
        $this->_path_js = 'controller/';
        $this->_judul = 'Pengelola';
        $this->_controller_name = 'b_pengelola';
        $this->_model_name = 'model_b_pengelola';
        $this->_path_upload = '../upload/pengelola/';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [$this->_path_js . 'b_pengelola'];
        $data['datas'] = $this->{$this->_model_name}->get_ref_table('ref_pengelola');
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [$this->_path_js . 'b_pengelola'];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [$this->_path_js . 'b_pengelola'];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['dokid' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->get_by_id('ref_pengelola', $key);

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $dokidOld = $this->input->post('dokidOld');
        $this->form_validation->set_rules('doknama', 'Nama', 'trim|xss_clean');
        $this->form_validation->set_rules('dokbidang', 'Bidang', 'trim|xss_clean');
        if (empty($_FILES['dokfoto']['name']) and empty($dokidOld))
            $this->form_validation->set_rules('dokfoto', 'dokfoto', 'trim|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $doknama = $this->input->post('doknama');
                $dokbidang = $this->input->post('dokbidang');
                $konfig = array(
                    'url'      => '../upload/dokter/',
                    'type'     => 'jpg|jpeg|png',
                    'size'     => 1024 * 8,
                    'namafile' => mt_rand(),
                    'name' => 'dokfoto'
                );

                if (!empty($_FILES['dokfoto']['name'])) {
                    $prosesUpload = uploadfile($konfig, $this->_model_name);
                }

                $param = array(
                    'doknama' => $doknama,
                    'dokbidang' => $dokbidang,
                    'dokfoto' =>  $prosesUpload['file_name'],

                );

                if (empty($dokidOld)) {
                    $proses = $this->{$this->_model_name}->insert('ref_dokter', $param);
                } else {
                    $key = array('dokid' => $dokidOld);
                    $proses = $this->{$this->_model_name}->update('ref_dokter', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['dokid' => $keyS];
        $proses = $this->{$this->_model_name}->delete('ref_dokter', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }

    public function loadimage()
	{
		$file = $this->uri->segment(3);
		ob_clean();
		$path = FCPATH . '../upload/dokter/'. $file;
		$size = getimagesize($path);
		header('Content-Type:' . $size['mime']);
		switch ($size['mime']) {
			case 'image/png':
			$img = imagecreatefrompng($path);

			imagepng($img);
			break;

			default:
			$img = imagecreatefromjpeg($path);
			imagejpeg($img);
			break;
		}
		imagedestroy($img);
	}
}
