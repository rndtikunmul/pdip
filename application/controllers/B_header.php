
<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class b_header extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/b_header/';
        $this->_path_js = null;
        $this->_judul = 'Header Website';
        $this->_controller_name = 'b_header';
        $this->_model_name = 'model_b_header';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->get_ref_table('f_header');
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['hdId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->get_by_id('f_header', $key);

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $hdIdOld = $this->input->post('hdIdOld');
        $this->form_validation->set_rules('hdWarna', 'hdWarna', 'trim|xss_clean');
        $this->form_validation->set_rules('hdLogo', 'hdLogo', 'trim|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $hdWarna = $this->input->post('hdWarna');
                $hdLogo = $this->input->post('hdLogo');


                $param = array(
                    'hdWarna' => $hdWarna,
                    'hdLogo' => $hdLogo,

                );

                if (empty($hdIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('f_header', $param);
                } else {
                    $key = array('hdId' => $hdIdOld);
                    $proses = $this->{$this->_model_name}->update('f_header', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['hdId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('f_header', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }
}
