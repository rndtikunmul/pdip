<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class berita extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->_controller_name = 'berita';
		$this->_model = 'model_f_master';
		$this->load->model('model_berita','',TRUE);
		$this->load->model('model_b_berita','',TRUE);
		$this->load->model($this->_model, '', TRUE);
	}

	public function index()
	{	
		$data['is_active'] = 'ragam';	
		$data['pages'] = 'page/berita/view';
		$header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
		$data['header'] = $header;
		$data['data'] = $this->model_berita->get(5);
		$data['all'] = $this->model_berita->get(5);
		$data['berita'] = $this->model_berita->get_berita('berita',5);
		// print_r($data['berita']);
		// exit();
		// $data['datas'] = $this->model_berita->get_berita();
        $data['menu'] = menu();
		$this->load->view('page/template', $data);
	}

	public function post($beritaNama)//single post page
    {
    	$data['is_active'] = 'beritapost';
        $data['pages'] = 'page/berita/post';
        $data['data'] = $this->model_berita->get(5);
		$data['datas'] = $this->model_berita->get_nama($beritaNama);
		$data['all'] = $this->model_berita->get(5);
		$header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
		$data['header'] = $header;
        $data['menu'] = menu();
        $this->load->view('page/template', $data);
	}

	public function loadimage()
	{
		$file = $this->uri->segment(3);
		ob_clean();
		$path = FCPATH . '../upload_file/berita/'. $file;
		$size = getimagesize($path);
		header('Content-Type:' . $size['mime']);
		switch ($size['mime']) {
			case 'image/png':
			$img = imagecreatefrompng($path);

			imagepng($img);
			break;

			default:
			$img = imagecreatefromjpeg($path);
			imagejpeg($img);
			break;
		}
		imagedestroy($img);
	}
	
	public function loadthumb()
	{
		$file = $this->uri->segment(3);
		ob_clean();
		$path = FCPATH . '../upload_file/berita/thumb/'. $file;
		$size = getimagesize($path);
		header('Content-Type:' . $size['mime']);
		switch ($size['mime']) {
			case 'image/png':
			$img = imagecreatefrompng($path);

			imagepng($img);
			break;

			default:
			$img = imagecreatefromjpeg($path);
			imagejpeg($img);
			break;
		}
		imagedestroy($img);
	}

}