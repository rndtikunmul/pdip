<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class galeri extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->_controller_name = 'galeri';
		$this->_model = 'model_galeri';
		$this->_path_js = 'home';
		$this->load->model($this->_model, '', TRUE);
		$this->load->model('model_f_master','',TRUE);

	}

	public function index()
	{		
		$data['is_active'] = 'ragam';
		$data['menu'] = menu();
		$data['pages'] = 'page/galeri/index';
		$header = $this->model_f_master->get_by_id('f_header',['hdId'=>1]);
		$data['header'] = $header;
		$data['data'] = $this->model_galeri->get_foto();
		$data['datafolder'] =$this->model_galeri->get_folder();
		$this->load->view('page/template', $data);
	}

	public function loadimage()
	{
		$file = $this->uri->segment(3);
		ob_clean();
		$path = FCPATH . '../upload_file/galeri/'. $file;
		$size = getimagesize($path);
		header('Content-Type:' . $size['mime']);
		switch ($size['mime']) {
			case 'image/png':
			$img = imagecreatefrompng($path);

			imagepng($img);
			break;

			default:
			$img = imagecreatefromjpeg($path);
			imagejpeg($img);
			break;
		}
		imagedestroy($img);
	}
	
	public function loadthumb()
	{
		$file = $this->uri->segment(3);
		ob_clean();
		$path = FCPATH . '../upload_file/galeri/thumb/'. $file;
		$size = getimagesize($path);
		header('Content-Type:' . $size['mime']);
		switch ($size['mime']) {
			case 'image/png':
			$img = imagecreatefrompng($path);

			imagepng($img);
			break;

			default:
			$img = imagecreatefromjpeg($path);
			imagejpeg($img);
			break;
		}
		imagedestroy($img);
	}
}