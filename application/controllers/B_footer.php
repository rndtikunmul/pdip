
<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class b_footer extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/b_footer/';
        $this->_path_js = null;
        $this->_judul = 'Footer Website';
        $this->_controller_name = 'b_footer';
        $this->_model_name = 'model_b_footer';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->get_ref_table('f_footer');
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['footId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->get_by_id('f_footer', $key);

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $footIdOld = $this->input->post('footIdOld');
        $this->form_validation->set_rules('footMap', 'footMap', 'trim|xss_clean');
        $this->form_validation->set_rules('footAlamat', 'footAlamat', 'trim|xss_clean');
        $this->form_validation->set_rules('footTlp', 'footTlp', 'trim|xss_clean');
        $this->form_validation->set_rules('footEmail', 'footEmail', 'trim|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $footMap = $this->input->post('footMap');
                $footAlamat = $this->input->post('footAlamat');
                $footTlp = $this->input->post('footTlp');
                $footEmail = $this->input->post('footEmail');


                $param = array(
                    'footMap' => $footMap,
                    'footAlamat' => $footAlamat,
                    'footTlp' => $footTlp,
                    'footEmail' => $footEmail,

                );

                if (empty($footIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('f_footer', $param);
                } else {
                    $key = array('footId' => $footIdOld);
                    $proses = $this->{$this->_model_name}->update('f_footer', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['footId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('f_footer', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }
}
