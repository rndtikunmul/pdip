<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class Jurnal extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->_controller_name = 'jurnal';
		$this->_model = 'model_f_master';
		$this->load->model('model_berita','',TRUE);
        $this->load->model('model_b_berita','',TRUE);
        $this->load->model('model_prodi','',TRUE);
		$this->load->model($this->_model, '', TRUE);
		$this->_path_page = 'page/jurnal/';
	}

	public function index()
    {
    	$data['is_active'] = 'jurnal';	
		$data['pages'] = 'page/jurnal/view';
		$header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
		$data['header'] = $header;
		$prodi = $this->{$this->_model}->get_prodi('f_jurusan');
		$data['prodi'] = $prodi;
		$lab = $this->{$this->_model}->get_lab('f_lab');
		$data['lab'] = $lab;
		$jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
		$data['jurnam'] = $jurnam;
		$data['data'] = $this->model_berita->get(5);
		$data['all'] = $this->model_berita->get(5);
		$data['berita'] = $this->model_berita->get_berita('BERITA, KEGIATAN',5);
		// $data['datas'] = $this->model_berita->get_berita();
        $data['menu'] = menu();
		$this->load->view('page/template', $data);
    }

	public function response()
    {
    	$jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
		$data['jurnam'] = $jurnam;
		$data['data'] = $this->model_berita->get(5);
        $data['menu'] = menu();
		$pages = $this->_path_page . 'response';
        $this->load->view($pages, $data);
    }

	public function post($labId)//single post page
    {
        $data['is_active'] = 'jurnal';
        $data['pages'] = 'page/jurnal/post';
        $data['datas'] = $this->model_prodi->get_lab_id($labId);
        $header = $this->{$this->_model}->get_by_id('f_header',['hdId'=>1]);
		$data['header'] = $header;
		$prodi = $this->{$this->_model}->get_prodi('f_jurusan');
        $data['prodi'] = $prodi;
        $lab = $this->{$this->_model}->get_lab('f_lab');
		$data['lab'] = $lab;
		$jurnam = $this->{$this->_model}->get_jurnam('f_jurnal_nama');
		$data['jurnam'] = $jurnam;
        $data['menu'] = menu();        
        // $data['kegiatan'] = $this->model_berita->get_berita('KEGIATAN',5);
		// $data['pengumuman'] = $this->model_berita->get_berita('PENGUMUMAN',5);
        $this->load->view('page/template', $data);
    }


}